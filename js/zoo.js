id_start = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><msub><mi>z</mi><mn>0</mn></msub><mo>=</mo><mi>c</mi></mrow><annotation encoding=\"TeX\">z_0 = c</annotation></semantics></math>"
zero_start = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><msub><mi>z</mi><mn>0</mn></msub><mo>=</mo><mn>0</mn></mrow><annotation encoding=\"TeX\">z_0 = 0</annotation></semantics></math>"
ones_start = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><msub><mi>z</mi><mn>0</mn></msub><mo>=</mo><mn>1</mn></mrow><annotation encoding=\"TeX\">z_0 = 1</annotation></semantics></math>"

function param_range(letter, p_min, p_max) {
	return () => {
		var result = [];
		for (var i = p_min; i < p_max; ++i) {
			result.push({disp:letter+"="+i, name:""+i});
		}
		return result;
	}
}

function params(letter, array) {
	return () => {
		var result = [];
		for (var i in array) {
			result.push({disp:letter+"="+array[i][0], name:array[i][1]});
		}
		return result;
	}
}

function join_params(p1, p2) {
	return () => {
		var result = [];
		var ps1 = p1();
		var ps2 = p2();
		for (i in ps1) {
			for (j in ps2) {
				result.push({disp:ps1[i].disp+"<br>"+ps2[j].disp, name:ps1[i].name+"_"+ps2[j].name});
			}
		}
		return result;
	}
}

function link(text, dest) {
	return "<a href=\"" + dest + "\">" + text + "</a>";
}

function link_class(text, dest) {
	return link(text, "class.html?class="+dest)
}

classes = {
	"multibrot": {
	name: "Multibrot",
	instance: "multibrot_2",
	formula: "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mi>z</mi><mo stretchy=\"false\">→</mo><msup><mi>z</mi><mi>n</mi></msup><mo>+</mo><mi>c</mi></mrow><annotation encoding=\"TeX\">z \\to z^n + c</annotation></semantics></math>",
	start: id_start,
	freedom: {"ℕ": 1},
	examples: param_range("n", 2, 6),
	description: "<p>For n=2, it is the classical Mandelbrot set. If n is negative, I call it "+link_class("antimultibrot", "antimultibrot")+".",
	},
	
	"antimultibrot": {
	name: "Antimultibrot",
	instance: "antimultibrot_2",
	formula: "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mi>z</mi><mo stretchy=\"false\">→</mo><msup><mi>z</mi><mrow><mo>−</mo><mi>n</mi></mrow></msup><mo>+</mo><mi>c</mi></mrow><annotation encoding=\"TeX\">z \\to z^{-n} + c</annotation></semantics></math>",
	start: id_start,
	freedom: {"ℕ": 1},
	examples: param_range("n", 2, 6),
	description: "<p>This is the case of "+link_class("multibrot","multibrot")+" for negative n.",
	},
	
	"cosine_multibrot": {
	name: "Cosine Multibrot",
	instance: "cosine_multibrot_1",
	formula: "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mi>z</mi><mo stretchy=\"false\">→</mo><mi>c</mi><mo>⋅</mo><msup><mo lspace=\"0em\" rspace=\"0em\">cos</mo><mi>n</mi></msup><mi>z</mi></mrow><annotation encoding=\"TeX\">z \\to c\\cdot\\cos^n{z}</annotation></semantics></math>",
	start: id_start,
	freedom: {"ℕ": 1},
	examples: param_range("n", 1, 4),
	description: "<p>The escape distance must be set to a very high number, else the strings are cut-off. For powers bigger than one, two types of "+link_class("multibrot","multibrot")+" shapes appear there. Classic Mandelbrot set and 2n-order multibrots. When <i>n</i> is an odd multiple of one half, the fractal is also Mandelbrot-like and contains multibrots of odd order but it is not connected. <p>If the parameter is negative, it is "+link_class("secans multibrot","secans_multibrot")+".",
	},

	"sine_multibrot": {
	name: "Sine Multibrot",
	instance: "sine_multibrot_1",
	formula: "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mi>z</mi><mo stretchy=\"false\">→</mo><mi>c</mi><mo>⋅</mo><msup><mo lspace=\"0em\" rspace=\"0em\">sin</mo><mi>n</mi></msup><mi>z</mi></mrow><annotation encoding=\"TeX\">z \\to c\\cdot\\sin^n{z}</annotation></semantics></math>",
	start: id_start,
	freedom: {"ℕ": 1},
	examples: param_range("n", 1, 4),
	description: "<p>The escape distance must be set to a very high number, else the strings are cut-off. <p>If the parameter is negative, it is "+link_class("cosecans multibrot","cosecans_multibrot")+".",
	},

	"secans_multibrot": {
	name: "Secans Multibrot",
	instance: "secans_multibrot_1",
	formula: "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mi>z</mi><mo stretchy=\"false\">→</mo><mi>c</mi><mo>⋅</mo><msup><mo lspace=\"0em\" rspace=\"0em\">sec</mo><mi>n</mi></msup><mi>z</mi></mrow><annotation encoding=\"TeX\">z \\to c\\cdot\\sec^n{z}</annotation></semantics></math>",
	start: id_start,
	freedom: {"ℕ": 1},
	examples: param_range("n", 1, 4),
	description: "<p>It is the inverse of "+link_class("cosine multibrot", "cosine_multibrot")+". The shapes appearing here look like infinite-order "+link_class("multibrots","multibrot")+". Usually, each neck connects two bodies but here the necks seem to connect 2n bodies. It is an interesting image showing that infinity times infinity times infinity still fits in a finite area",
	},

	"cosecans_multibrot": {
	name: "Cosecans Multibrot",
	instance: "cosecans_multibrot_1",
	formula: "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mi>z</mi><mo stretchy=\"false\">→</mo><mi>c</mi><mo>⋅</mo><msup><mo lspace=\"0em\" rspace=\"0em\">csc</mo><mi>n</mi></msup><mi>z</mi></mrow><annotation encoding=\"TeX\">z \\to c\\cdot\\csc^n{z}</annotation></semantics></math>",
	start: id_start,
	freedom: {"ℕ": 1},
	examples: param_range("n", 1, 4),
	description: "<p>It is the inverse of "+link_class("sine multibrot", "sine_multibrot")+". Usually, the fractals using inverse exponents are very dense but this one is not.",
	},
	
	"logobrot": {
	name: "Logobrot",
	instance: "logobrot_1_1.0",
	formula: "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mi>z</mi><mo stretchy=\"false\">→</mo><mi>c</mi><mo>⋅</mo><msup><mi>z</mi><mrow><mi>q</mi><mo>⋅</mo><msup><mo lspace=\"0em\" rspace=\"0em\">ln</mo><mi>n</mi></msup><mi>z</mi></mrow></msup></mrow><annotation encoding=\"TeX\">z \\to c \\cdot z^{q\\cdot\\ln^n{z}}</annotation></semantics></math>",
	start: id_start,
	freedom: {"ℂ": 1, "ℕ": 1},
	examples: join_params(param_range("n", 1, 6), params("q", [["0.5", "0.5"], ["0.7", "0.7"], ["1.0", "1.0"]])),
	description: "<p>The logarithm function is not connected in ℂ, so the fractal looks like being cut at some places. When there is a minibrot near the cut, some of its parts can be cut off. The parameter <i>q</i> can be an arbitrary complex number but when it is too small, the fractal may collapse into a huge distorted mandelbrot. This seems to be caused by that the cuts appear more often and the mandelbrots are growing as <i>q</i> approaches the origin. And when <i>q</i> is too small, the mandelbrots become so huge and cut at so many places, so they look more like one huge distorted mandelbrot.",
	},

	"approx_cosine_multibrot": {
	name: "Approximate Cosine Mandelbrot",
	instance: "approx_cosine_multibrot_2_1",
	formula: "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mi>z</mi><mo stretchy=\"false\">→</mo><mi>c</mi><mo>⋅</mo><msup><mrow><mo>(</mo><mrow><mn>1</mn><mo>−</mo><mfrac><msup><mi>z</mi><mi>n</mi></msup><mi>n</mi></mfrac></mrow><mo>)</mo></mrow><mi>m</mi></msup></mrow><annotation encoding=\"TeX\">z \\to c\\cdot\\left(1 - \\frac{z^n}{n} \\right)^m</annotation></semantics></math>",
	start: id_start,
	freedom: {"ℤ": 2},
	examples: join_params(params("n", [["-3", "-3"], ["-2", "-2"], ["-1", "-1"], ["2", "2"], ["3", "3"], ["4", "4"]]), params("m", [["-3", "-3"], ["-2", "-2"], ["-1", "-1"], ["1", "1"], ["2", "2"], ["3", "3"]])),
	description: "<p>The formula comes from the Taylor expansion of cosinus. The shapes for n=2 look like the central shape in the "+link_class("cosine multibrot", "cosine_multibrot")+".",
	},

	"approx_sine_multibrot": {
	name: "Approximate Sine Mandelbrot",
	instance: "approx_sine_multibrot_3_1",
	formula: "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mi>z</mi><mo stretchy=\"false\">→</mo><mi>c</mi><mo>⋅</mo><msup><mrow><mo>(</mo><mrow><mi>z</mi><mo>−</mo><mfrac><msup><mi>z</mi><mi>n</mi></msup><mi>n</mi></mfrac></mrow><mo>)</mo></mrow><mi>m</mi></msup></mrow><annotation encoding=\"TeX\">z \\to c\\cdot\\left(z - \\frac{z^n}{n} \\right)^m</annotation></semantics></math>",
	start: ones_start,
	freedom: {"ℤ": 2},
	examples: join_params(params("n", [["-3", "-3"], ["-2", "-2"], ["-1", "-1"], ["2", "2"], ["3", "3"], ["4", "4"]]), params("m", [["-3", "-3"], ["-2", "-2"], ["-1", "-1"], ["1", "1"], ["2", "2"], ["3", "3"]])),
	description: "<p>The formula comes from the Taylor expansion of sinus. The shapes for n=2 look like the central shape in the "+link_class("sine multibrot", "sine_multibrot")+".",
	},
}

function browse_classes() {
	document.write("<center><br>");
	var height = "200px"
	for (var i in classes) {
		document.write("<a href=\"class.html?class=" + i + "\">");
		document.write("<table class=\"icons\" width=\"" + height + "\">");
		document.write("<tr><td align=center valign=middle height=\"" + height + "\">");
		document.write("<img src=\"");
		document.write("img/" + classes[i].instance + "_overview.png");
		document.write("\" style=\"max-height:" + height + "; max-width=" + height + ";\"></td></tr><tr><th>");
		document.write(classes[i].name);
		document.write("</td></tr></table></a>");
	}
	document.write("<br clear=all></center>");
}

function show_details() {
	var url = new URL(window.location.href);
	var c = url.searchParams.get("class");
	document.write("<table class=image border=1><tr><th colspan=2>" + classes[c].name + "</th></tr>");

	document.write("<tr><td colspan=2><a href=\"img/" + classes[c].instance + "_overview.png\">");

	document.write("<img src=\"img/" + classes[c].instance + "_overview.png\" width=\"300px\">");
	document.write("</a></td></tr>");
	document.write("<tr><td rowspan=2>formula</td><td>" + classes[c].start + "</td></tr><tr><td>" + classes[c].formula + "</td></tr>");
	document.write("<tr><td>degrees of freedom</td><td>");
	var str = "";
	for (var i in classes[c].freedom) {
		if (str != "") {
			str = str + " + ";
		}
		str = str + classes[c].freedom[i] + i;
	}
	document.write(str + "</td></tr>");
	document.write("</table>");
	
	document.write("<h1 class=noclear>" + classes[c].name + "</h1>");
	document.write(classes[c].description);
	
	document.write("<br clear=all>");
	document.write("<table align=center><tr><th>Parameters</th><th>Overview</th><th>Detail</th><th>Riemann Sphere</th>");
	examples = classes[c].examples()
	for (var i in examples) {
		var overview = "img/" + c + "_" + examples[i].name + "_overview.png"
		var detail = "img/" + c + "_" + examples[i].name + "_detail.png"
		var sphere = "img/" + c + "_" + examples[i].name + "_sphere.png"
		document.write("<tr><td>"+examples[i].disp+"</td>");
		document.write("<td><a href=\"" + overview + "\"><img src=\"" + overview + "\" width=\"200px\"></a></td>");
		document.write("<td><a href=\"" + detail + "\"><img src=\"" + detail + "\" width=\"200px\"></a></td>");
		document.write("<td><a href=\"" + sphere + "\"><img src=\"" + sphere + "\" width=\"200px\"></a></td>");
		document.write("</tr>")
	}
	document.write("</table>")
}

